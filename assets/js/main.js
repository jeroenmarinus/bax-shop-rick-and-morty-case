// Disable other filters
document.querySelectorAll('#searchform select').forEach(function (element) {
  element.addEventListener('change', function () {
    document.querySelectorAll('#searchform select').forEach(function (select) {
      if (element !== select) {
        select.setAttribute('disabled', 'disabled');
      }
    });
  });
});

// Clear filters
document.querySelector('.clear-filter').addEventListener('click', function () {
  console.log('zefez')
  document.querySelectorAll('#searchform select').forEach(function (element) {
    element.value = '';
    element.removeAttribute('disabled');
  });
});
