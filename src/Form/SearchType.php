<?php

namespace App\Form;

use App\Service\RickAndMortyApiService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    private RickAndMortyApiService $api;

    public function __construct(RickAndMortyApiService $api)
    {
        $this->api = $api;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dimension', ChoiceType::class, [
                'required' => false,
                'choices' => $this->api->getDimensionChoices(),
                'data' => $options['search']['dimension'] ?? null,
            ])
            ->add('location', ChoiceType::class, [
                'required' => false,
                'choices' => $this->api->getLocationChoices(),
                'data' => $options['search']['location'] ?? null,
            ])
            ->add('episode', ChoiceType::class, [
                'required' => false,
                'choices' => $this->api->getEpisodeChoices(),
                'data' => $options['search']['episode'] ?? null,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'search' => [],
        ]);
    }
}
