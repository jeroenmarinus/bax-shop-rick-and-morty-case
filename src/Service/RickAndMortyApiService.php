<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\HttpOptions;
use Symfony\Contracts\Cache\ItemInterface;

class RickAndMortyApiService
{
    private const CACHE_EXPIRATION = 3600;
    private string $graphQlEndpoint = 'https://rickandmortyapi.com/graphql';

    public function __construct()
    {
    }

    public function search(array $search = []): array
    {
        $query = 'query {characters {info {pages},results {id,name,status,species,location {id,name},image,episode {id,episode, name}}}}';

        if (!empty($search['dimension'])) {
            $query = 'query {locations(filter: {dimension: "'.$search['dimension'].'"}) {info {pages}, results {id, name, residents{id,name,status,species,location {id,name},image,episode {id,episode, name}}}}}';
        }

        if (!empty($search['location'])) {
            $query = 'query {locationsByIds(ids: ['.$search['location'].']) {id, name, residents{id,name,status,species,location {id,name},image,episode {id,episode, name}}}}';
        }

        if (!empty($search['episode'])) {
            $query = 'query {episodesByIds(ids: ['.$search['episode'].']) {id, name, characters{id,name,status,species,location {id,name},image,episode {id,episode, name}}}}';
        }

        $results = $this->graphqlQuery($this->graphQlEndpoint, $query);

        if (!empty($search['dimension'])) {
            $characters = [];
            foreach ($results['data']['locations']['results'] as $location) {
                $characters = array_merge($location['residents'], $characters);
            }

            return $characters;
        }

        if (!empty($search['location'])) {
            $characters = [];
            foreach ($results['data']['locationsByIds'] as $location) {
                $characters = array_merge($location['residents'], $characters);
            }

            return $characters;
        }

        if (!empty($search['episode'])) {
            $characters = [];
            foreach ($results['data']['episodesByIds'] as $episode) {
                $characters = array_merge($episode['characters'], $characters);
            }

            return $characters;
        }

        return $results['data']['characters']['results'] ?? [];
    }

    public function getCharacter(int $id)
    {
        $query = 'query {charactersByIds(ids: ['.$id.']) {id,name,status,species,type,gender,origin{id,name,dimension},location{id,name,dimension},image,episode{name,air_date,episode}}}';

        $result = $this->graphqlQuery($this->graphQlEndpoint, $query);

        return $result['data']['charactersByIds'][0];
    }

    public function getDimensionChoices(): array
    {
        // This is an expensive operation, so we'll cache the result to reduce the amount of requests and processing
        $cache = new FilesystemAdapter();

        return $cache->get('dimension_choices', function (ItemInterface $item) {
            $infoQuery = 'query {locations {info {count, pages}}}';

            $info = $this->graphqlQuery($this->graphQlEndpoint, $infoQuery);
            $pages = $info['data']['locations']['info']['pages'];

            $choices = [];
            for ($i = 1; $i <= $pages; ++$i) {
                $dataQuery = "query {locations(page:${i}) {results {dimension}}}";
                $data = $this->graphqlQuery($this->graphQlEndpoint, $dataQuery);
                $dimensionNames = array_column($data['data']['locations']['results'], 'dimension');
                $dimensions = array_combine($dimensionNames, $dimensionNames);
                $choices = array_merge($choices, $dimensions);
            }

            $item->expiresAfter(self::CACHE_EXPIRATION);

            $filtered = array_filter($choices);
            ksort($filtered);

            return $filtered;
        });
    }

    public function getLocationChoices(): array
    {
        // This is an expensive operation, so we'll cache the result to reduce the amount of requests and processing
        $cache = new FilesystemAdapter();

        return $cache->get('location_choices', function (ItemInterface $item) {
            $infoQuery = 'query {locations {info {count, pages}}}';

            $info = $this->graphqlQuery($this->graphQlEndpoint, $infoQuery);
            $pages = $info['data']['locations']['info']['pages'];

            $choices = [];
            for ($i = 1; $i <= $pages; ++$i) {
                $dataQuery = "query {locations(page:${i}) {results {id, name}}}";
                $data = $this->graphqlQuery($this->graphQlEndpoint, $dataQuery);
                $locations = array_combine(array_column($data['data']['locations']['results'], 'name'), array_column($data['data']['locations']['results'], 'id'));
                $choices = array_merge($choices, $locations);
            }

            $item->expiresAfter(self::CACHE_EXPIRATION);

            $filtered = array_filter($choices);
            ksort($filtered);

            return $filtered;
        });
    }

    public function getEpisodeChoices(): array
    {
        // This is an expensive operation, so we'll cache the result to reduce the amount of requests and processing
        $cache = new FilesystemAdapter();

        return $cache->get('episode_choices', function (ItemInterface $item) {
            $infoQuery = 'query {episodes {info {count, pages}}}';

            $info = $this->graphqlQuery($this->graphQlEndpoint, $infoQuery);
            $pages = $info['data']['episodes']['info']['pages'];

            $choices = [];
            for ($i = 1; $i <= $pages; ++$i) {
                $dataQuery = "query {episodes(page:${i}) {results {id, name}}}";
                $data = $this->graphqlQuery($this->graphQlEndpoint, $dataQuery);
                $episodes = array_combine(array_column($data['data']['episodes']['results'], 'name'), array_column($data['data']['episodes']['results'], 'id'));
                $choices = array_merge($choices, $episodes);
            }

            $item->expiresAfter(self::CACHE_EXPIRATION);

            $filtered = array_filter($choices);
            asort($filtered);

            return $filtered;
        });
    }

    public function graphqlQuery(string $endpoint, string $query, array $variables = [], ?string $token = null): array
    {
        $options = (new HttpOptions())
            ->setJson([
                'query' => $query,
                'variables' => $variables,
            ])
            ->setHeaders([
                'Content-Type' => 'application/json',
                'User-Agent' => 'Symfony GraphQL client',
            ]);

        if (null !== $token) {
            $options->setAuthBearer($token);
        }

        return HttpClient::create()
            ->request('POST', $endpoint, $options->toArray())
            ->toArray();
    }
}
