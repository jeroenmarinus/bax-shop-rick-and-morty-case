<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Service\RickAndMortyApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(Request $request, RickAndMortyApiService $api, SessionInterface $session): Response
    {
        $search = $request->query->all('search');
        if (!$search) {
            $search = $session->get('search', []);
        }
        $session->set('search', $search);

        $searchForm = $this->createForm(SearchType::class, null, [
            'search' => $search,
        ]);

        $characters = $api->search($search);

        return $this->render('default/index.html.twig', [
            'characters' => $characters,
            'search_form' => $searchForm->createView(),
        ]);
    }

    #[Route('/character/{id}', name: 'character_detail')]
    public function characterDetail(int $id, RickAndMortyApiService $api)
    {
        $character = $api->getCharacter($id);

        return $this->render('default/character_detail.html.twig', [
            'character' => $character,
        ]);
    }
}
