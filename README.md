# Bax Shop Rick & Morty Case
By Jeroen Marinus

## Getting Started

1. Run `composer install`
2. Run `yarn install`
3. Run `yarn run build`
4. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/)
5. Run `docker-compose build --pull --no-cache` to build fresh images
6. Run `docker-compose up` (the logs will be displayed in the current shell)
7. Open `https://localhost` in your favorite web browser and [accept the auto-generated TLS certificate](https://stackoverflow.com/a/15076602/1352334)
8. Run `docker-compose down --remove-orphans` to stop the Docker containers.

### Missing features/improvements
- Pagination: Didn't have enough time to implement a pagination based on the API 
- RickAndMortyApiService->search() method code quality. I think the current implementation is not very pretty but I didn't have enough time to look at how to restructure it/make it more readable/efficient
- IMDB/TVDB link in character detail for more info about episodes.
